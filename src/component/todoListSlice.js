import { createSlice, current } from "@reduxjs/toolkit";
import { act } from "react-dom/test-utils";

const todoListSlice = createSlice({
    name: 'todo',
    initialState: {
        todoList: [],
    },
    reducers: {
        addTodo: (state, action) => {
            const todo = action.payload;
            state.todoList = [...state.todoList, todo];
        },
        removeTodo: (state, action) => {
            state.todoList = state.todoList.filter((todo) => {
                return action.payload !== todo.id
            })
        },
        changeStatus: (state, action) => {
            state.todoList.forEach(todo => {
                if (todo.id === action.payload) {
                  todo.done = !todo.done
                }
            })        
        }
    },
});
export const { addTodo, removeTodo,changeStatus } = todoListSlice.actions;
export default todoListSlice.reducer;
