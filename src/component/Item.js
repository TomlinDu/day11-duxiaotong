import './Item.css'
import { useDispatch } from 'react-redux';
import { removeTodo,changeStatus } from './todoListSlice';
function Item(props) {
    const { todo } = props;
    const dispatch = useDispatch();
    const handleRemoveTodo = () => {
        dispatch(removeTodo(
            todo.id
        ));
    }

    return (
        <div className='item'>
            <p className='todoText' onClick={() => {dispatch(changeStatus(todo.id))}} style={{textDecoration: todo.done ? 'line-through' : 'none'}}>
                {todo.text}
            </p>
            <button onClick={handleRemoveTodo}>X</button>
        </div>
    )
}
export default Item;
