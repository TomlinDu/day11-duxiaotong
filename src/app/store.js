import { configureStore } from '@reduxjs/toolkit'
import todoListReducer from '../component/todoListSlice';

export default configureStore({
  reducer: {
    todo:todoListReducer
  }
});